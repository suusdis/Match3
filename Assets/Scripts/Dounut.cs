﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dounut : MonoBehaviour {

    //WahHed direction you swipe
    private Vector2 s_FirstTouchPosition;
    private Vector2 s_FinalTouchPosition;
    private float s_SwipeAngle = 0;

    [Header("Row and colom of dounut")]
    [SerializeField] private int s_Colom;
    public int s_Row;
    [SerializeField] private int s_OldColom;
    [SerializeField] private int s_OldRow;
    private int s_TargetX;
    private int s_TargetY;
    public bool s_IsMatched = false;

    //neightbor dounut
    private GameObject s_Neightbor;
    private GridScript s_Grid;

    private Vector2 s_TargetPosition;
    //stops if you click you automatic swipe right
    private float s_SwipeResist = 1f;

    public void Initialize(int x, int y)
    {
        s_Grid = FindObjectOfType<GridScript>();
        s_TargetX = x;
        s_TargetY = y;
        s_Row = s_TargetY;
        s_Colom = s_TargetX;
        s_OldRow = s_Row;
        s_OldColom = s_Colom;
    }
    private void Update()
    {
        CheckForMatched();
        if (s_IsMatched)
        {
            SpriteRenderer ThisSprite = GetComponent<SpriteRenderer>();
            ThisSprite.color = new Color(0, 0, 0, .2f);
            //s_Grid.DestoryMatches();
        }

        s_TargetX = s_Colom;
        s_TargetY = s_Row;

        //left and right swipe
        if(Mathf.Abs(s_TargetX - transform.position.x) > .1f)
        {
            //towards target 
            s_TargetPosition = new Vector2(s_TargetX, transform.position.y);
            transform.position = Vector2.Lerp(transform.position, s_TargetPosition, .4f);
        }
        else
        {
            //directly set
            s_TargetPosition = new Vector2(s_TargetX, transform.position.y);
            transform.position = s_TargetPosition;
            s_Grid.s_Alldounuts[s_Colom, s_Row] = this.gameObject;
        }
        //up and down swipe
        if (Mathf.Abs(s_TargetY - transform.position.y) > .1f)
        {
            //towards target 
            s_TargetPosition = new Vector2(transform.position.x, s_TargetY);
            transform.position = Vector2.Lerp(transform.position, s_TargetPosition, .4f);
        }
        else
        {
            //directly set
            s_TargetPosition = new Vector2(transform.position.x, s_TargetY);
            transform.position = s_TargetPosition;
            s_Grid.s_Alldounuts[s_Colom, s_Row] = this.gameObject;
        }
    }

    private void OnMouseDown()
    {
        s_FirstTouchPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
    }
    private void OnMouseUp()
    {
        s_FinalTouchPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        CalculateAngle();
    }
    void CalculateAngle()
    {
        //makes sure you cant tap and not swipe
        if(Mathf.Abs(s_FinalTouchPosition.y - s_FirstTouchPosition.y) > s_SwipeResist || Mathf.Abs(s_FinalTouchPosition.x - s_FirstTouchPosition.x) > s_SwipeResist)
        {
            //angle you swipe
            s_SwipeAngle = Mathf.Atan2(s_FinalTouchPosition.y - s_FirstTouchPosition.y, s_FinalTouchPosition.x - s_FirstTouchPosition.x) * 180 / Mathf.PI;
            MoveDounuts();
        }
    }
    void MoveDounuts()
    {
        if(s_SwipeAngle > -45 && s_SwipeAngle <= 45 && s_Colom +1 < s_Grid.s_Width)
        {
            //when you swipe to right
            s_Neightbor = s_Grid.s_Alldounuts[s_Colom + 1, s_Row];
            s_Neightbor.GetComponent<Dounut>().s_Colom -= 1;
            s_Colom += 1;
           
        }
        else if (s_SwipeAngle > 45 && s_SwipeAngle <= 135 && s_Row < s_Grid.s_Height)
        {
            //when you swipe to up
            s_Neightbor = s_Grid.s_Alldounuts[s_Colom, s_Row + 1];
            s_Neightbor.GetComponent<Dounut>().s_Row -= 1;
            s_Row += 1;
            
        }
        else if ((s_SwipeAngle > 135 || s_SwipeAngle <= -135) && s_Colom > 0)
        {
            //when you swipe to Left
            s_Neightbor = s_Grid.s_Alldounuts[s_Colom - 1, s_Row];
            s_Neightbor.GetComponent<Dounut>().s_Colom += 1;
            s_Colom -= 1;
            
        }
        else if (s_SwipeAngle < -45 && s_SwipeAngle >= -135 && s_Row > 0)
        {
            //when you swipe to Down
            s_Neightbor = s_Grid.s_Alldounuts[s_Colom, s_Row - 1];
            s_Neightbor.GetComponent<Dounut>().s_Row += 1;
            s_Row -= 1;          
        }
        StartCoroutine(CheckMoveCo());
    }
    void CheckForMatched()
    {
        //left and right
        if(s_Colom > 0 && s_Colom < s_Grid.s_Width -1)
        {
            //check left and right neightbors
            GameObject Left = s_Grid.s_Alldounuts[s_Colom - 1, s_Row];
            GameObject Right = s_Grid.s_Alldounuts[s_Colom + 1, s_Row];
            if (Left != null && Right != null)
            {
                if (Left.tag == this.gameObject.tag && Right.tag == this.gameObject.tag)
                {
                    Left.GetComponent<Dounut>().s_IsMatched = true;
                    Right.GetComponent<Dounut>().s_IsMatched = true;
                    s_IsMatched = true;
                }
            }
        }
        if (s_Row > 0 && s_Row < s_Grid.s_Height - 1)
        {
            //check left and right neightbors
            GameObject Up = s_Grid.s_Alldounuts[s_Colom, s_Row + 1];
            GameObject Down = s_Grid.s_Alldounuts[s_Colom, s_Row - 1];
            if (Up != null && Down != null)
            {
                if (Up.tag == this.gameObject.tag && Down.tag == this.gameObject.tag)
                {
                    Up.GetComponent<Dounut>().s_IsMatched = true;
                    Down.GetComponent<Dounut>().s_IsMatched = true;
                    s_IsMatched = true;
                }
            }
        }
    }
    public IEnumerator CheckMoveCo()
    {
        yield return new WaitForSeconds(.5f);
        if (s_Neightbor != null)
        {
            // if there is no match
            if (!s_IsMatched && !s_Neightbor.GetComponent<Dounut>().s_IsMatched)
            {
                s_Neightbor.GetComponent<Dounut>().s_Row = s_Row;
                s_Neightbor.GetComponent<Dounut>().s_Colom = s_Colom;
                s_Row = s_OldRow;
                s_Colom = s_OldColom;
            }
            else
            {
                s_Grid.DestoryMatches();
            }
            s_Neightbor = null;      
        }
        

    }
}
