﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridScript : MonoBehaviour {

    public int s_Width;
    public int s_Height;
    [SerializeField] private GameObject s_TilePrefab;
    private BackgroundCell[,] s_Grid;

    [Header("Array Dounuts")]
    [SerializeField] private GameObject[] s_Dounuts;
    public GameObject[,] s_Alldounuts;


    void Start ()
    {
        s_Grid = new BackgroundCell[s_Width, s_Height];
        s_Alldounuts = new GameObject[s_Width, s_Height];
        SetUpGrid();
	}


    void SetUpGrid()
    {
        for (int i = 0; i < s_Width; i++)
        {
            for (int j = 0; j < s_Height; j++)
            {
                //spawn tiles
                Vector2 PositionTiles = new Vector2(i, j);
                GameObject BackgroundTile = Instantiate(s_TilePrefab, PositionTiles, Quaternion.identity) as GameObject;
                BackgroundTile.transform.parent = this.transform;
                BackgroundTile.name = "(" + i + ", " + j + ")";
                int AmountDounuts = Random.Range(0, s_Dounuts.Length);

                //itterate if dounuts needs to changes
                int AmountItterations = 0;
                while(SpawnNoMatches(i, j, s_Dounuts[AmountDounuts]) && AmountItterations < 100)
                {
                    AmountDounuts = Random.Range(0, s_Dounuts.Length);
                    AmountItterations++;
                }
                AmountItterations = 0;

                //instanciate dounuts
                GameObject dounut = Instantiate(s_Dounuts[AmountDounuts], PositionTiles, Quaternion.identity);
                dounut.transform.parent = this.transform;
                dounut.name = "(" + i + ", " + j + ")" + "Dounut";
                s_Alldounuts[i, j] = dounut;
                dounut.GetComponent<Dounut>().Initialize(i, j);
            }
        }
    }

    public bool SpawnNoMatches(int colom, int row, GameObject pieces)
    {
        //check if same tag after 1
        if (colom > 1 && row > 1)
        {
            if(s_Alldounuts[colom -1, row].tag == pieces.tag && s_Alldounuts[colom - 2, row].tag == pieces.tag)
            {
                return true;
            }
            if (s_Alldounuts[colom, row-1].tag == pieces.tag && s_Alldounuts[colom, row-2].tag == pieces.tag)
            {
                return true;
            }
        }
        //check if same tag before 1
        else if(colom <= 0 || row <= 0)
        {
            if(row > 1)
            {
                if(s_Alldounuts[colom, row - 1].tag == pieces.tag && s_Alldounuts[colom, row - 2].tag == pieces.tag)
                {
                    return true;
                }
            }
            if (colom > 1)
            {
                if (s_Alldounuts[colom -1, row].tag == pieces.tag && s_Alldounuts[colom - 2, row].tag == pieces.tag)
                {
                    return true;
                }
            }
        }
        return false;
    }

    private void DestroyMathesAt(int colom, int row)
    {
        if(s_Alldounuts[colom, row].GetComponent<Dounut>().s_IsMatched)
        {
            Destroy(s_Alldounuts[colom, row].gameObject);
            //FallDounut();
        }
    }
    public void DestoryMatches()
    {
        for (int i = 0; i < s_Width; i++)
        {
            for (int j = 0; j < s_Height; j++)
            {
                if (s_Alldounuts[i, j] != null)
                {
                    DestroyMathesAt(i, j);
                }
            }
        }
        StartCoroutine(FallDounut());
    }
    public IEnumerator FallDounut()
    {
        yield return new WaitForSeconds(.4f);
        int nullCount = 0;
        for (int i = 0; i < s_Width; i++)
        {
            for (int j = 0; j < s_Height; j++)
            {
                if(s_Alldounuts[i,j].gameObject == null)
                {
                    nullCount++;
                }
                else if(nullCount > 0)
                {
                    s_Alldounuts[i, j].GetComponent<Dounut>().s_Row -= nullCount;
                }
            }
            nullCount = 0;
        }
    }
}
